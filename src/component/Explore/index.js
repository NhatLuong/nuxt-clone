import React from "react";
import t_styles from "assets/styles/style.module.scss";
import { EXPLORES } from "utils/share/staticData";
import styles from "./style.module.scss";
export default function Explore() {
  return (
    <>
      <div className={t_styles.wrapper}>
        <h3 className={`${t_styles.t_blur} ${t_styles.t_md}`}>Explore </h3>
        <h4 className={`${t_styles.t_xlg} ${t_styles.t_700}`}>
          Moving forward? So much to <b className={t_styles.t_gr}> Explore </b>
        </h4>
        <p className={t_styles.t_xmd}>
          Discover powerful modules, integrate with your favorite providers and
          start quickly with themes.
        </p>
        <div className={styles.container}>
          {EXPLORES.map((explore, index) => (
            <div key={index} className={styles.content}>
              <img src={explore.img} alt="" />
              <p className={`${t_styles.t_700} ${t_styles.t_xmd}`}>
                {explore.title}
              </p>
              <p>{explore.des}</p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
