import React from "react";
import t_styles from "assets/styles/style.module.scss";
import styles from "./style.module.scss";
import { TESTIMONIALS } from "utils/share/staticData";
export default function Testimonials() {
  return (
    <>
      <div className={t_styles.wrapper}>
        <h3 className={`${t_styles.t_gr} ${t_styles.t_md}`}>Community</h3>
        <h4 className={`${t_styles.t_700} ${t_styles.t_xlg}`}>Testimonials</h4>
        <p className={`${t_styles.t_xmd}`}>
          Learn what the experts love about Nuxt.
        </p>
        <div className={styles.container}>
          {TESTIMONIALS.map((tes, index) => (
            <div key={index} className={styles.card}>
              <p>{tes.des}</p>
              <div className={styles.info}>
                <img className={styles.avatar} src={tes.avatar} alt="" />
                <div className={styles.profile}>
                  <p className={`${t_styles.t_md} ${t_styles.t_700}`}>
                    {tes.name}
                  </p>
                  <p className={`${t_styles.t_blur} ${t_styles.t_xsm}`}>
                    {tes.title}
                  </p>
                </div>
                <img className={styles.company} src={tes.in} alt="" />
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
