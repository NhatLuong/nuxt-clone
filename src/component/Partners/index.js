import React from "react";
import t_styles from "assets/styles/style.module.scss";
import { PARTNERS } from "utils/share/staticData";
import styles from "./style.module.scss";
import Button from "component/Button";
export default function Partners() {
  return (
    <>
      <div className={t_styles.wrapper}>
        <h3 className={`${t_styles.t_blur} ${t_styles.t_md}`}>Partners</h3>
        <h4 className={`${t_styles.t_xlg} ${t_styles.t_700}`}>
          Sustainable<b className={t_styles.t_gr}> Development</b>
        </h4>
        <p className={t_styles.t_xmd}>
          Nuxt development is carried out by passionate developers, but the
          amount of effort needed to maintain and develop new features is not
          sustainable without proper financial backing. We are thankful for our
          sponsors and partners, who help make Nuxt possible.
        </p>
        <div className={styles.container}>
          {PARTNERS.map((partner, index) => (
            <div key={index} className={styles.content}>
              <img src={partner.img} alt="" />
              <p>{partner.title}</p>
              <p>{partner.des}</p>
              <Button>{partner.to}</Button>
            </div>
          ))}
        </div>
        <Button variant="outlined">Become a partner</Button>
      </div>
    </>
  );
}
