import React from "react";
import styles from "./style.module.scss";
import t_styles from "assets/styles/style.module.scss";
import { FEATURES } from "utils/share/staticData";

export default function Features() {
  return (
    <>
      <div className={t_styles.wrapper}>
        <h3 className={`${t_styles.t_blur} ${t_styles.t_md}`}>Features</h3>
        <h4 className={`${t_styles.t_700} ${t_styles.t_xlg}`}>
          Intuitive <b className={t_styles.t_gr}>D</b>eveloper E
          <b className={t_styles.t_gr}>x</b>perience
        </h4>
        <p className={`${t_styles.t_xmd}`}>
          Nuxt is shipped with plenty of features to boost developer
          productivity and the end user experience.
        </p>
        <div className={styles.contents}>
          {FEATURES.map((feat, index) => (
            <div key={index} className={styles.contents_item}>
              <img src={feat.img} alt="" />
              <p className={`${t_styles.t_xmd} ${t_styles.t_700}`}>
                {feat.title}
              </p>
              <p className={t_styles.t_sm}>{feat.des}</p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
