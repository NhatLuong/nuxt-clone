import React from "react";
import styles from "./style.module.scss";
import { AiFillStar, AiFillGithub } from "react-icons/ai";
import Button from "component/Button";
import t_styles from "assets/styles/style.module.scss";

export default function Banner() {
  return (
    <div className={t_styles.wrapper}>
      <span className={styles.announ}>
        <AiFillStar /> <b>Announcement:</b> Nuxt 3 Release Candidate is out!
      </span>
      <h3 className={`${t_styles.t_xxlg} ${t_styles.t_700} ${t_styles.m_0}`}>
        The Intuitive Vue
      </h3>
      <h3 className={`${t_styles.t_xxlg} ${t_styles.t_700}`}>Framework</h3>
      <h4 className={`${t_styles.t_xmd} ${t_styles.t_blur} ${t_styles.m_0}`}>
        Build your next Vue.js application with confidence using Nuxt.
      </h4>
      <h4 className={`${t_styles.t_xmd} ${t_styles.t_blur}`}>
        An open source framework making web development simple and powerful.
      </h4>
      <div className={styles.btn_wrapper}>
        <a>
          <AiFillGithub />
          40K+ GitHub stars
        </a>
        <Button>Get started</Button>
      </div>
    </div>
  );
}
