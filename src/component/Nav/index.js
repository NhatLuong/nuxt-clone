import React from "react";
import logo from "assets/imgs/logo.svg";
import menu from "assets/imgs/menu.svg";
import style from "./style.module.scss";
import { RiArrowDownSLine, RiSearchLine } from "react-icons/ri";
import { TbLanguageHiragana } from "react-icons/tb";
import t_styles from "assets/styles/style.module.scss";
import { NAV } from "utils/share/staticData";
import { useMediaQuery } from "hooks/useMediaQuery";
export default function Nav() {
  const matches = useMediaQuery(1024);
  return (
    <div className={style.nav_wrapper}>
      <div className={style.nav}>
        {matches ? (
          <>
            <img width={20} src={menu} alt="" />
            <img src={logo} alt="" />
            <RiSearchLine style={{ width: 20, height: 20 }} />
          </>
        ) : (
          <>
            <img src={logo} alt="" />
            <ul>
              {NAV.map((ele, index) => (
                <li
                  key={index}
                  className={`${t_styles.t_700} ${t_styles.t_sm}`}
                >
                  {ele.title} <RiArrowDownSLine />
                </li>
              ))}
            </ul>
            <div>
              <div>
                <TbLanguageHiragana />
                <RiArrowDownSLine />
              </div>
              <RiSearchLine />
            </div>
          </>
        )}
      </div>
    </div>
  );
}
