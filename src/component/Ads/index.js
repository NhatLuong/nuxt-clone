import React from "react";
import style from "./style.module.scss";
export default function Ads() {
  return (
    <p className={style.ads}>
      <b>Nuxt 3 Release Candidate </b> is out! Discover more about it on{" "}
      <b>v3.nuxtjs.org</b>
    </p>
  );
}
