import Ads from "./Ads";
import Testimonials from "./Testimonials";
import Partners from "./Partners";
import Nav from "./Nav";
import Learn from "./Learn";
import Guide from "./Guide";
import Footer from "./Footer";
import Features from "./Features";
import Explore from "./Explore";
import Community from "./Community";
import Banner from "./Banner";
import Button from "./Button";
import Container from "./Container";
export {
  Features,
  Ads,
  Testimonials,
  Nav,
  Partners,
  Footer,
  Learn,
  Guide,
  Explore,
  Community,
  Banner,
  Button,
  Container,
};
