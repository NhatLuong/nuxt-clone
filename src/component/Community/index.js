import React from "react";
import t_styles from "assets/styles/style.module.scss";
import styles from "./style.module.scss";
import { COMUNITIES } from "utils/share/staticData";
import { AiOutlineRight } from "react-icons/ai";
export default function Community() {
  return (
    <>
      <div className={t_styles.wrapper_start}>
        <h3 className={`${t_styles.t_gr} ${t_styles.t_md}`}>Community</h3>
        <h4 className={`${t_styles.t_700} ${t_styles.t_xlg}`}>
          Sharing is <b className={t_styles.t_sky}>Caring</b>
        </h4>
        <p className={`${t_styles.t_xmd}`}>
          Discover articles from the framework team and community about Nuxt.
          Tips and tricks included!
        </p>
        <div className={styles.container}>
          {COMUNITIES.map((community, index) => (
            <div key={index} className={styles.content}>
              <div>
                <img src={community.img} alt="" />
              </div>
              <p className={`${t_styles.t_gr} ${t_styles.t_md}`}>
                {community.name}
              </p>
              <p className={`${t_styles.t_700} ${t_styles.t_xmd}`}>
                {community.title}
              </p>
              <p className={t_styles.t_md}>
                {community.des.slice(0, 74) + "..."}
              </p>
              <p className={styles.link}>
                Get Infos <AiOutlineRight />
              </p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
