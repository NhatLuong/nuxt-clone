import React from "react";
import styles from "./style.module.scss";
import t_styles from "assets/styles/style.module.scss";
import { FOOTERS } from "utils/share/staticData";
import Button from "component/Button";
import tw from "assets/imgs/tw.svg";
import dis from "assets/imgs/dis.svg";
import git from "assets/imgs/git.svg";

export default function Footer() {
  return (
    <div className={styles.container}>
      {FOOTERS.map((footer, index) => (
        <ul key={index}>
          <li
            className={`${t_styles.t_blur} ${t_styles.t_700} ${t_styles.t_md}`}
          >
            {footer.title}
          </li>
          {footer.childrend.map((chil, i) => (
            <li className={`${t_styles.t_md}`} key={chil + i}>
              {chil}
            </li>
          ))}
        </ul>
      ))}
      <div className={styles.contact}>
        <p className={`${t_styles.t_blur} ${t_styles.t_700} ${t_styles.t_md}`}>
          NewsLetter
        </p>
        <p>
          The latest news, articles, and resources, sent to your inbox monthly.
        </p>
        <div className={styles.form}>
          <input placeholder="Email" />
          <Button>Subcribe</Button>
        </div>
        <div>
          <a href="/">
            <img src={tw} alt="" />
          </a>
          <a href="/">
            <img src={dis} alt="" />
          </a>
          <a href="/">
            <img src={git} alt="" />
          </a>
        </div>
      </div>
    </div>
  );
}
