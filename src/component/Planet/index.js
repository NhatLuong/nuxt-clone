import React from "react";
import styles from "./style.module.scss";

export default function Planet() {
  return (
    <div className={styles.planet}>
      <img
        src="https://nuxtjs.org/img/home/hero/gem-1.svg"
        alt="An image of a green gem from nuxt galaxy"
      />
      <img
        src="https://nuxtjs.org/img/home/hero/gem-2.svg"
        alt="An image of a green gem from nuxt galaxy"
      />
      <img
        src="https://nuxtjs.org/img/home/hero/gem-4.svg"
        alt="An image of a green gem from nuxt galaxy"
      />
      <img
        src="https://nuxtjs.org/img/home/hero/gem-3.svg"
        alt="An image of a green gem from nuxt galaxy"
      />
      <img
        src="https://nuxtjs.org/img/home/hero/gem-2.svg"
        alt="An image of a green gem from nuxt galaxy"
      />
      <img
        src="https://nuxtjs.org/img/home/hero/gem-2.svg"
        alt="An image of a green gem from nuxt galaxy"
      />
    </div>
  );
}
