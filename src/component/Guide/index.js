import React from "react";
import t_styles from "assets/styles/style.module.scss";
import styles from "./style.module.scss";
import { GUIDES } from "utils/share/staticData";
export default function Guide() {
  return (
    <>
      <div className={t_styles.wrapper}>
        <h3 className={`${t_styles.t_blur} ${t_styles.t_md}`}>Learn </h3>
        <h4 className={`${t_styles.t_xlg} ${t_styles.t_700}`}>
          Follow our <b className={t_styles.t_gr}> Guides</b>
        </h4>
        <p className={t_styles.t_xmd}>
          From an idea to a masterpiece, guides take you on the path to becoming
          a Nuxter.
        </p>
        <div className={styles.container}>
          {GUIDES.map((guide, index) => (
            <div className={styles.content} key={index}>
              <img src={guide.img} alt="" />
              <p className={`${t_styles.t_700} ${t_styles.t_xmd}`}>
                {guide.title}
              </p>
              <p className={t_styles.t_sm}>{guide.des}</p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
