import React from "react";
import styles from "./style.module.scss";
import t_styles from "assets/styles/style.module.scss";
import content_1 from "assets/imgs/content_1.svg";
import content_2 from "assets/imgs/content_2.svg";
import content_3 from "assets/imgs/content_3.svg";
import Button from "component/Button";

export default function Learn() {
  return (
    <>
      <div className={styles.wrapper}>
        <h3 className={`${t_styles.t_blur} ${t_styles.t_md}`}>Learn</h3>
        <h4 className={`${t_styles.t_xlg} ${t_styles.t_700}`}>
          <b className={t_styles.t_gr}>Easy</b> to learn.{" "}
          <b className={t_styles.t_gr}>Easy</b> to master
        </h4>
        <p className={t_styles.t_xmd}>
          Learn everything you need to know, from beginner to master.
        </p>
        <div className={styles.tab}>
          <span>
            From CLI<span className={t_styles.active_tab}></span>
          </span>
          <span>From Scratch</span>
        </div>
        <div className={styles.tab_content}>
          <div className={styles.tab_content_ide}>
            <div className={styles.tab_content_ide_header}>
              <div>
                <span></span>
                <span></span>
                <span></span>
              </div>
              <p className={t_styles.t_sm}>index.vue</p>
            </div>
            <div className={styles.tab_content_ide_main}>
              <ul className={styles.tab_content_ide_main_index}>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li>6</li>
                <li>7</li>
                <li>8</li>
                <li>9</li>
              </ul>
              <ul className={styles.tab_content_ide_main_code}>
                <li>&lt;template&gt;</li>
                <li style={{ marginLeft: 5 }}>&lt;div&gt;</li>
                <li style={{ marginLeft: 10 }}>&lt;Logo/&gt;</li>
                <li style={{ marginLeft: 10 }}>&lt;NuxtCard/&gt;</li>
                <li style={{ marginLeft: 10 }}>&lt;NuxtCard/&gt;</li>
                <li style={{ marginLeft: 10 }}>&lt;AlertBanner/&gt;</li>
                <li style={{ marginLeft: 5 }}>&lt;div/&gt;</li>
                <li>&lt;template&gt;</li>
                <li></li>
              </ul>
            </div>
          </div>
          <div className={styles.tab_content_screen}>
            <img src={content_1} />
            <img src={content_2} />
            <img src={content_3} />
          </div>
        </div>
        <div className={styles.btns}>
          <Button>Start learning</Button>
        </div>
      </div>
    </>
  );
}
