import React from "react";
import style from "./style.module.scss";
function Button({ children, variant }) {
  return (
    <button
      className={variant === "outlined" ? style.outlined : style.contained}
    >
      {children}
    </button>
  );
}

export default Button;
