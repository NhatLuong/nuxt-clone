export const FEATURES = [
  {
    title: "Zero Configuration",
    img: "https://nuxtjs.org/img/home/discover/dx/zero-config.svg",
    des: "Start coding your app right away, Nuxt takes care of the rest.",
  },
  {
    title: "File-system Routing",
    img: "https://nuxtjs.org/img/home/discover/dx/file-system-routing.svg",
    des: "Automatic routing and code-splitting for every page.",
  },
  {
    title: "Rendering Modes",
    img: "https://nuxtjs.org/img/home/discover/dx/hybrid.svg",
    des: "Switch between static-site generation or on-demand server rendering.",
  },
  {
    title: "Data Fetching",
    img: "https://nuxtjs.org/img/home/discover/dx/fetch.svg",
    des: "Fetch your content from any source in your Vue components, SSR ready.",
  },
  {
    title: "Strong Conventions",
    img: "https://nuxtjs.org/img/home/discover/dx/conventions.svg",
    des: "Efficient teamwork with a strong directory structure and conventions.",
  },
  {
    title: "SEO Friendly",
    img: "https://nuxtjs.org/img/home/discover/dx/seo.svg",
    des: "Meta tag management and faster time-to-content for great indexing.  ",
  },
  {
    title: "Components Auto-import",
    img: "	https://nuxtjs.org/img/home/discover/dx/auto-inject.svg",
    des: "Use your components, Nuxt will import them with smart code-splitting.",
  },
  {
    title: "Modules Ecosystem",
    img: "https://nuxtjs.org/img/home/discover/dx/modular.svg",
    des: "Extend your app with 160+ Nuxt modules and create your own.",
  },
];

export const PARTNERS = [
  {
    img: "https://nuxtjs.org/img/partners/categories/technology.svg",
    title: "Technology partners",
    des: "Technology partners offer services that empower Nuxt developers, such as CMS, Hosting, Database, and more",
    to: "Discover our technology partners",
  },
  {
    img: "	https://nuxtjs.org/img/partners/categories/agency.svg",
    title: "Agency partners",
    des: "Agency partners are trusted web and consulting agencies that can provide Nuxt development and support for your projects.",
    to: "Find a Nuxt expert",
  },
];

export const GUIDES = [
  {
    title: "Documentation",
    des: "Discover Nuxt concepts and find a complete API reference.",
    img: "https://nuxtjs.org/img/home/learn/guides/gem-1.svg",
  },
  {
    title: "Examples",
    des: "Learn by examples produced by the community.",
    img: "https://nuxtjs.org/img/home/learn/guides/gem-2.svg",
  },
  {
    title: "Releases",
    des: "Find out what has changed before upgrading.",
    img: "https://nuxtjs.org/img/home/learn/guides/gem-3.svg",
  },
  {
    title: "Master Courses",
    des: "Watch a complete series of videos to learn Nuxt with our partner Vue School.",
    img: "https://nuxtjs.org/img/home/learn/guides/gem-4.svg",
  },
];

export const EXPLORES = [
  {
    title: "Deployments",
    des: "Extend and automate your workflow by using deployments for your favorite tools.",
    img: "https://nuxtjs.org/img/home/explore/gem-explore-1.svg",
  },
  {
    title: "Modules",
    des: "Discover our list of modules to supercharge your Nuxt project. Created by the Nuxt team and commun",
    img: "https://nuxtjs.org/img/home/explore/gem-explore-2.svg",
  },
  {
    title: "Themes",
    des: "See how a real world application is built using the Nuxt stack with the themes built by our partners.",
    img: "https://nuxtjs.org/img/home/explore/gem-explore-3.svg",
  },
];

export const COMUNITIES = [
  {
    name: "Announcements",
    title: "Nuxt Static Improvements",
    des: "With Nuxt version 2.13, the full-static mode has been introduced. In additioLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum ",
    img: "https://nuxtjs.org/_nuxt/image/b07ac2.jpeg",
  },
  {
    name: "Events",
    title: "Nuxt 3 - The Beginners Workshop",
    des: "In this workshop, we will develop a near-real-life app based on Nuxt and VuLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    img: "https://nuxtjs.org/_nuxt/image/f787ca.jpg",
  },
];

export const TESTIMONIALS = [
  {
    name: "Evan You",
    des: "Nuxt offers a compelling solution and a great ecosystem to help you ship fullstack Vue apps that are performant and SEO friendly. The flexibility to choose between SSR and SSG is icing on the cake.",
    title: "Creator of Vue.js",
    avatar: "https://nuxtjs.org/img/home/testimonials/evan.png",
    in: "https://nuxtjs.org/img/home/testimonials/vue.svg",
  },
  {
    name: "Sarah Drasner",
    des: `Nuxt has outstanding developer productivity, experience, and performance right out of the gate!
  There’s so much attention to detail, ensuring teams have everything at their fingertips to productively build all manners of applications.',
  title:'Core Team of Vue.js`,
    title: "Core Team of Vue.js",
    avatar: "https://nuxtjs.org/img/home/testimonials/sarah.png",
    in: "https://nuxtjs.org/img/home/testimonials/vue.svg",
  },
  {
    name: "Addy Osmani",
    des: "Nuxt is a fantastic choice for teams building a production-grade product on the web. It aims to bake in performance best-practices while maintaining excellent Vue.js DX.",
    title: "Chief Engineer of Chrome",
    avatar: "https://nuxtjs.org/img/home/testimonials/addy.png",
    in: "https://nuxtjs.org/img/home/testimonials/chrome.svg",
  },
  {
    name: "Guillermo Rauch",
    des: "Nuxt has been an incredible source of innovation and inspiration for developers and framework authors alike. It’s been amazing to see its growth in web projects of all sizes on the web.",
    title: "Founder of Vercel",
    avatar: "https://nuxtjs.org/img/home/testimonials/guillermo.png",
    in: "https://nuxtjs.org/img/home/testimonials/vercel-light.svg",
  },
  {
    name: "Dominik Angerer",
    des: "Nuxt has a unique approach of combining a great developer experience with reusable, fully integrated features that speed up the development and performance of your next website or application.",
    title: "Founder of Storyblok",
    avatar: "https://nuxtjs.org/img/home/testimonials/dominik.png",
    in: "https://nuxtjs.org/img/home/testimonials/storyblok.svg",
  },
  {
    name: "Sadek Drobi",
    des: "Nuxt is our primary choice for offering a seamless website development experience to our users. Its simplicity and progressive learning curve makes it our ideal choice for SliceMachine.",
    title: "Founder of Prismic",
    avatar: "https://nuxtjs.org/img/home/testimonials/sadek.png",
    in: "https://nuxtjs.org/img/home/testimonials/prismic.svg",
  },
  {
    name: "Ajay Kapur",
    des: `Every serious full stack development team needs to stop take a look at Nuxt. Vue's developer productivity combined with Nuxt's server side rendering, is the foundation for instant loading web sites that delight users and improve team velocity.`,
    title: "Founder of Layer0",
    avatar: "https://nuxtjs.org/img/home/testimonials/ajay.png",
    in: "https://nuxtjs.org/img/home/testimonials/layer0-light.svg",
  },
  {
    name: "Dave Loneragan",
    des: "Nuxt has an ideal balance of approachability for developers new to JAMstack, and power for experienced teams working on complex applications. The modules and first-class integration with the rest of the Vue ecosystem makes for a superb DX.",
    title: "Co-founder of Swell",
    avatar: "https://nuxtjs.org/img/home/testimonials/dave.png",
    in: "https://nuxtjs.org/img/home/testimonials/swell.svg",
  },
  {
    name: "Savas Vedova",
    des: "The moment I used Nuxt for the first time I felt in love with it. Apart from its scalability, performance and developer experience, the team behind of it is also fantastic. Thanks for developing such a great framework and making our lives much easier!",
    title: "Founder of Stormkit",
    avatar: "https://nuxtjs.org/img/home/testimonials/savas.png",
    in: "https://nuxtjs.org/img/home/testimonials/stormkit.svg",
  },
];

export const FOOTERS = [
  {
    title: "About",
    childrend: [
      "Contact us",
      "Enterprise support",
      "NuxtLabs company",
      "Open Source Software",
      "Partnerships",
      "Telemetr",
    ],
  },
  {
    title: "Ecosystem",
    childrend: [
      "Announcements",
      "Contribute",
      "Chat with us",
      "Events",
      "Sponsors",
      "Teams",
      "Tutorials",
      "Video courses",
    ],
  },
  {
    title: "Resources",
    childrend: [
      "Design",
      "Documentation",
      "Examples",
      "Deployments",
      "Master courses",
      "Modules",
      "Releases",
      "Showcases",
      "Themes",
    ],
  },
];

export const NAV = [
  {
    title: "Discover",
    childrend: [
      {
        title: "Showcases",
        des: "Selection of websites built with Nuxt",
        img: "https://nuxtjs.org/img/header/showcases.svg",
      },
      {
        title: "Showcases",
        des: "Selection of websites built with Nuxt",
        img: "https://nuxtjs.org/img/header/case-studies.svg",
      },
      {
        title: "Showcases",
        des: "Selection of websites built with Nuxt",
        img: "https://nuxtjs.org/img/header/testimonials.svg",
      },
    ],
  },
  {
    title: "Learn",
    childrend: [
      {
        title: "Docs",
        img: "https://nuxtjs.org/img/header/docs.svg",

        des: "Create fast websites easily",
      },
      {
        title: "Examples",
        img: "https://nuxtjs.org/img/header/examples.svg",

        des: "Understand everything in Nuxt",
      },
      {
        title: "Tutorials",
        img: "https://nuxtjs.org/img/header/tutorials.svg",

        des: "Learn with concrete cases",
      },
      {
        title: "Master courses",
        img: "https://nuxtjs.org/img/header/master-courses.svg",

        des: "Learn more with experts",
      },
    ],
  },
  {
    title: "Explore",
    childrend: [
      {
        title: "Deployments",
        des: "How to Deploy Nuxt",
        img: "https://nuxtjs.org/img/header/deployments.svg",
      },
      {
        title: "Modules",
        des: "Extend the power of Nuxt",
        img: "https://nuxtjs.org/img/header/modules.svg",
      },
      {
        title: "Themes",
        des: "Get started with themes",
        img: "https://nuxtjs.org/img/header/themes.svg",
      },
      {
        title: "Video Courses",
        des: "Learn step by step",
        img: "https://nuxtjs.org/img/header/video-courses.svg",
      },
    ],
  },
  {
    title: "Community",
    childrend: [
      {
        title: "Announcements",
        des: "Latest news about Nuxt",
        img: "https://nuxtjs.org/img/header/announcements.svg",
      },
      {
        title: "Teams",
        des: "They are Nuxt",
        img: "https://nuxtjs.org/img/header/teams.svg",
      },
      {
        title: "Releases",
        des: "All the code we have released",
        img: "https://nuxtjs.org/img/header/releases.svg",
      },
      {
        title: "Sponsors  ",
        des: "They trust us",
        img: "https://nuxtjs.org/img/header/sponsors.svg",
      },
    ],
  },
  {
    title: "Partner",
  },
];
