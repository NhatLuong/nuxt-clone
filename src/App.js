import "./App.css";
import {
  Features,
  Ads,
  Testimonials,
  Nav,
  Partners,
  Footer,
  Learn,
  Guide,
  Explore,
  Community,
  Banner,
  Container,
} from "./component";
import styles from "assets/styles/style.module.scss";
import Star from "component/Star";
import t_styles from "assets/styles/style.module.scss";
import landscape from "assets/imgs/landscape.svg";
import landscape_white from "assets/imgs/landscape_white.svg";
import Planet from "component/Planet";

function App() {
  return (
    <>
      <Ads />
      <Nav />
      <div className={`${styles.black_background} ${styles.mt_72}`}>
        <Star />
        <Planet />
        <Container>
          <Banner />
          <img
            className={t_styles.img}
            style={{ marginTop: "-13rem" }}
            src={landscape}
            alt=""
          />
        </Container>
      </div>
      <div className={styles.light_black_background}>
        <Container>
          <Learn />
          <img className={t_styles.img} src={landscape_white} alt="" />
        </Container>
      </div>
      <div className={styles.no_background}>
        <Container>
          <Features />
          <img
            style={{
              transform: "translatey(15%)",
            }}
            className={t_styles.img}
            src="https://nuxtjs.org/img/home/discover/dx/discover-mountain.svg"
            alt=""
          />
        </Container>
      </div>
      <div className={styles.sky_background}>
        <Container>
          <Partners />
          <img
            style={{
              transform: "translatey(15%)",
            }}
            className={t_styles.img}
            src="https://nuxtjs.org/img/home/discover/partners/partners-illustration.svg"
            alt=""
          />
        </Container>
      </div>
      <div className={styles.black_background}>
        <Container>
          <Guide />
          <img
            className={t_styles.img}
            src="https://nuxtjs.org/img/home/explore/landscape-explore.svg"
            alt=""
          />
        </Container>
      </div>
      <div className={styles.no_background}>
        <Container>
          <Explore />
          <img
            style={{
              transform: "translatey(3%)",
            }}
            className={t_styles.img}
            src="https://nuxtjs.org/img/home/campfire/campfire-illustration-big.svg"
            alt=""
          />
        </Container>
      </div>
      <div className={styles.light_black_background}>
        <Container>
          <Community />
        </Container>
      </div>
      <div className={styles.no_background}>
        <Container>
          <Testimonials />
          <img
            style={{
              transform: "translatey(0%)",
            }}
            className={t_styles.img}
            src="https://nuxtjs.org/img/home/home_footer.svg"
            alt=""
          />
        </Container>
      </div>
      <div className={styles.light_black_background}>
        <Container>
          <Footer />
        </Container>
      </div>
    </>
  );
}

export default App;
